import React from "react"
import Layout from "../components/layout"
import Wave from "../components/Wave/Wave"

const WavePage = () => {
  return (
    <Layout>
      <Wave />
    </Layout>
  )
}

export default WavePage
