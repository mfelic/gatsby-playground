import React from "react"
import dev_activity from "../../images/dev_activity.svg"
import styles from "./home.module.css"

const Home = () => {
  return (
    <section className={styles.home}>
      <img src={dev_activity} alt="developer coding" />
      <h1>Many Pages</h1>
      <p>Double click to show pages</p>
    </section>
  )
}

export default Home
