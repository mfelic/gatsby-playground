/**
 * Layout component that queries for data
 * with Gatsby's StaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/static-query/
 */

import React, { useState } from "react"
import PropTypes from "prop-types"

import Sidebar from "./Sidebar/Sidebar"
import "./layout.css"

const Layout = ({ children }) => {
  const [isSidebarVisible, setSidebarVisibility] = useState(false)

  return (
    <>
      <Sidebar
        isSidebarVisible={isSidebarVisible}
        setSidebarVisibility={setSidebarVisibility}
      />
      <main
        onDoubleClick={() => {
          setSidebarVisibility(!isSidebarVisible)
        }}
      >
        {children}
      </main>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
