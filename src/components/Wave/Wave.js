import React from 'react';
import styles from './wave.module.css';

const Wave = () => {
  return (
    <>
      <section className={styles.wave}>
        <div className={styles.heading}>
          <h1>SurfSeeker</h1>
          <h3>Find Gnarly Waves Nearby</h3>
          <p>Realtime notifications of surf conditions in your area</p>
        </div>
        <div className={styles.registration}>
          <input type="email" placeholder="Enter your email address" />
          <button>Sign Up</button>
        </div>
      </section>
    </>
  );
};

export default Wave;
