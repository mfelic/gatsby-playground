import React from 'react';
import { Link } from 'gatsby';
import pages from '../../constants/pageList';
import './sidebar.css';

const Sidebar = props => {
  let attachedClasses = props.isSidebarVisible
    ? ['sidebar', 'open']
    : ['sidebar', 'close'];

  return (
    <>
      <nav className={attachedClasses.join(' ')}>
        <h3 className="sidebar__title">Pick a Page</h3>
        <ul className="sidebar__items">
          {pages.map((page, idx) => (
            <Link key={idx} to={page.path}>
              <li>{page.name}</li>
            </Link>
          ))}
        </ul>
      </nav>
    </>
  );
};

export default Sidebar;
